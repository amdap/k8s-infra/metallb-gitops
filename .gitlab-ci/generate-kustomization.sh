#!/usr/bin/env bash

set -x

source .version
echo "${METALLB_VERSION}"

mkdir --parents $(git rev-parse --show-toplevel)/.k8s/manifests
cp --recursive $(git rev-parse --show-toplevel)/k8s/* $(git rev-parse --show-toplevel)/.k8s
cd $(git rev-parse --show-toplevel)/.k8s/manifests
rm -rf *.yaml
url="https://raw.githubusercontent.com/metallb/metallb/$METALLB_VERSION/config/manifests/metallb-native.yaml"
curl -fsSLO "${url}"

r=$(kubectl-slice --skip-non-k8s -f metallb-native.yaml -o ./ 2>&1)
rm -rf metallb-native.yaml

cat <<'EOF' > kustomization.yaml
---
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
EOF

IFS="bytes." readarray -t lines <<< "$r"
for line in "${lines[@]}"; do
  echo "$line" | grep -v 'files generated.' | awk '{print "- " $2}' >> kustomization.yaml
done

cd $(git rev-parse --show-toplevel)
